Nginx RTMP stream manager
-------------------------

Simple API to be used by the nginx rtmp module's `notify` functions.

How it works
------------

The [nginx-rtmp-module](https://github.com/arut/nginx-rtmp-module/) has several directives that can configure nginx to send HTTP requests on specific events.

The ones this program handles are:

- [on_publish](https://github.com/arut/nginx-rtmp-module/wiki/Directives#on_publish)
- [on_publish_done](https://github.com/arut/nginx-rtmp-module/wiki/Directives#on_publish_done)
- [on_update](https://github.com/arut/nginx-rtmp-module/wiki/Directives#on_update)

If nginx receives anything other than a 2xx response from any of these requests the stream is disconnected (or not allowed to start in the first place).

When a new stream connects to nginx on port 1935 the `on_publish` event is triggered. We use this to verify that the key is valid, and take action accordingly. The full flow is:

- Check the method in the request is `POST`, if not then fail.
- Check that the form data in the request can be parsed, if not then fail.
- Check that the form contains the `name` parameter, if not then fail. This is set to the stream key that the client has configured by nginx.
- Check the key is in our list of keys, if not then fail.
- Check if there are any keys in the `PENDING_START` state:
    - If the key from the request is the key in this state:
        - Check there is currently an active key, if there is then fail
        - Check if there is a cooldown active on the key, if there is then fail
        - Otherwise set this key to the `LIVE` state and accept the connection
    - If the key from the request is not in this state:
         - If the pending cooldown has not expired, then fail
         - If the pending cooldown has expired, move the pending key to the `IDLE` state and continue processing.
- Check if there is an active key:
    - If the key in the request is not the active key, check its priority:
        - If the priority of the incoming key is higher than the active key, the active key is moved to the `PENDING_DISCONNECT` state, a 5 minute cooldown is added to it and a disconnection is requested.
        - So the stream is properly disconnected before the incoming one can start, its state is set to `PENDING_START` and the pending cooldown is set to 1 minute from now. We then wait for a few seconds until we get confirmation that the previous active key has disconnected, then set the state to `LIVE` and accept the connection. If we don't get confirmation in time then *the connection is  rejected* with the expectation that the client will retry within the next minute.
        - If the priority of the incoming key is lower or the same as the active key, then fail.
- If there is not an active key, check if the incoming key is in cooldown:
    - If it is then fail
    - If not accept the connection

Any failure results in the connection being refused.

The `on_publish_done` handler is triggered when the stream is closed by nginx. This is also triggered when we disconnect the stream. All it does is move the key its targeted at to the `IDLE` state.

The `on_update` handler is triggered every 5 seconds when a stream is active. If there are no streams going through nginx it is not used.

We use this primarily to handle disconnects after nginx has been reloaded. Unfortunately the rtmp control module loses all information about ongoing streams when nginx is reloaded, so when we request a disconnection we set the state to `PENDING_DISCONNECT` and next time the `on_update` handler is called we return an error so the stream is dropped.

We also use it when the program starts to keep track of the active stream. It will move one key from the `IDLE` to `LIVE` state, but only on the first update request it receives after starting.

How this works for users
------------------------

Users connect with OBS as usual. If there's no stream currently live, they will connect first attempt and start streaming.

If there's already a stream live with a lower priority than their key, it will be dropped in the background before they're allowed to connect, so it may take a few seconds to connect. If the client times out and produces an error, retrying will allow them to connect straight away.

If the stream that's live is of a higher or equal priority to their key, the connection will fail.

The idea behind this is to have a constanst 24/7 stream going with a "holding" stream going when other people aren't streaming, and to allow people to jump on to the stream without any admin intervention. It's not the greatest user experience with the need to retry but it means there's only ever one stream being pushed at a time. It also means that after they disconnect, as long as the hold stream is configured to constantly retry it will pick back up again.

Building
--------

To build this program you need [Go](https://golang.org), at least 1.23.1. Once you have that set up, simply run:

    go build

This will create the binary executable file `stream-key-manage`.

You can run the unit tests with:

    go test ./...


API Usage
---------

- List stream keys

    ```
    GET /api/key/
    ```

- Find keys with a given discord ID

    ```
    GET /api/key/?discord_id={id}
    ```

If any keys match the given discord ID a list of these keys will be returned. If no matching keys are found it will return 404 Not Found.

- Find keys with a given nick (display name)

    ```
    GET /api/key/?nick={nick}
    ```

If any keys match the given nick then a list of these keys will be returned. If no matching keys are found it will return 404 Not Found.

- Show the active stream key

    ```
    GET /api/key/active
    ```

n.b. this is a Read-only endpoint. If no keys are active it will return 404 Not Found.


- Generate a stream key

    ```
    POST /api/key/

    POSTDATA, as JSON:
        nick       (string): Nick of the user the key belongs to. Must be set.
        priority      (int): Priority of the key, a higher priority will boot an active key with a lower priority. Optional.
        discord_id (string): The key owner's ID on discord, for use by discord bots. Optional.
    ```

On success the call will return 202 Accepted.

Example response:

    {"id":"b0592bb1-c2e8-465e-a565-f667f3d6a76c","cooldown":"0001-01-01T00:00:00Z","priority":0,"state":"IDLE","nick":"user1"}


- Update a key:

    ```
    PUT /api/key/{{key}}
    ```

The data you send is the same as for creating the key, only the fields `nick` and `priority` can be changed. On success the call will return 202 Accepted.

Example:

    PUT /api/key/b0592bb1-c2e8-465e-a565-f667f3d6a76c
    {"priority": 100}

You can use this endpoint to remove a discord ID from a key. As setting it to 0 is the same as not sending it in the request, setting it to 0 will not change the value stored with the key. To remove it you have to set it to a non-zero value that is not a valid ID. The scheme used to create the IDs is [Snowflake](https://github.com/twitter-archive/snowflake/tree/snowflake-2010), which uses the first 41 bits of an unsigned 64 bit number to hold the date, so the easiest way to set it to an invalid value is to not set any of these bits, e.g. by using `"1"`. Note that this field is a string representation of the number to be consistent with discord, see the [entry on Snowflakes in the discord reference docs](https://discord.com/developers/docs/reference#snowflakes).

- Drop the connection for a key

    ```
    PUT /api/key/{{key}}/disconnect
    ```

Example:

    PUT /api/key/db10bc58-3d31-4217-aefb-953117a83f51/disconnect

You can also set a "cooldown" time, which will make that key unavailable for the duration of that cooldown.

Example to time it out for five minutes:


    curl -fu username:password http://example.com/api/key/db10bc58-3d31-4217-aefb-953117a83f51/disconnect -X PUT -d '{"cooldown":"5m"}'

Additionally, you can force a disconnect, this won't wait for nginx to finish the disconnect and will set the key straight to the IDLE state. I wouldn't recommend using this unless you really had to!

    curl -fu username:password http://example.com/api/key/db10bc58-3d31-4217-aefb-953117a83f51/disconnect -X PUT -d '{"force":true}'

- Reset a key

    ```
    PUT /api/key/{{key}}/reset
    ```

This will remove any cooldowns associated with a key and set it to the default state of `IDLE`. This won't work if the key is currently live, it will need to be disconnected before it can be reset.

This shouldn't need to be used but is there just in case the key gets in a weird state, e.g. if a long cooldown is set accidentally.

Example:

    PUT /api/key/db10bc58-3d31-4217-aefb-953117a83f51/reset


- Delete a key.

    ```
    DELETE /api/key/{{key}}
    ```

Example:

    DELETE /api/key/db10bc58-3d31-4217-aefb-953117a83f51

*n.b. You cannot delete an active key, if you try it will return "423 Locked". The key will need to be disconnected before it can be deleted*

Nginx config
------------

This requires the `rtmp_control` settings to be active on http port 8008, and at least `on_publish` and `on_publish_done` notifications:

    http {
        # Server used for RTMP control so we can disconnect sessions
        server {
            listen 127.0.0.1:8008;
            location /control {
                rtmp_control all;
            }

            location / {
                # not required but potentially useful
                rtmp_stat all;
                rtmp_stat_stylesheet stat.xsl;
            }

            # stat.xsl is in the root of the nginx-rtmp-module repo, custom ones can also be used
            location = /stat.xsl {
            }
        }

        # Server used as an intermediate for update calls so we can stop stream-key-manage
        # without disconnecting ongoing streams
        #
        server {
            listen 127.0.0.1:8088 default_server;

            location /update {
                proxy_pass http://127.0.0.1:8000;
                error_page 502 =200 /fakeok;
            }

            location = /fakeok { return 200; }
        }
    }

    rtmp {
        server {
            listen 127.0.0.1:1935;

            application live {
                live on;
                push rtmp://live.restream.io/live/RESTREAM_KEY_HERE;
                on_publish http://127.0.0.1:8000/publish;
                on_publish_done http://127.0.0.1:8000/publish_done;

                # note that on_update uses a different port, see the http block above
                on_update http://127.0.0.1:8088/update;
                notify_update_timeout 4s;
            }
        }
    }

Note that these internally called enpoints don't start with `/api/`. This is so they're not exposed publicly.
