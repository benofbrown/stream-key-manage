all: stream-key-manage

test:
	go test -cover ./...

stream-key-manage: main.go $(wildcard internal/skm/*.go)
	go build
