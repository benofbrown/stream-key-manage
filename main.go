package main

import (
	"flag"
	"gitlab.com/benofbrown/stream-key-manage/internal/skm"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var listenPort string
	var sessionPath string
	var discordConfigPath string

	flag.StringVar(&listenPort, "port", "8000", "port to listen on")
	flag.StringVar(&sessionPath, "session-file", "session.json", "file to save sessions to/load sessions from")
	flag.StringVar(&discordConfigPath, "discord-config", "discord.json", "discord configuration for webhook - optional")

	flag.Parse()

	keys := skm.Keys{}

	skm.LoadSessions(sessionPath, &keys)
	sessionLog := skm.LogSetup("log/session.log")
	hup := make(chan os.Signal, 1)
	signal.Notify(hup, syscall.SIGHUP)

	go func() {
		for {
			<-hup
			sessionLog.Rotate()
		}
	}()

	disconnectQueue := skm.RTMPDisconnectChannel("http://127.0.0.1:8008")

	pendingClear := make(chan bool, 4)
	publishHandler := skm.NewPublishHandler(&keys, disconnectQueue, pendingClear)
	if discordConfigFile, err := os.Open(discordConfigPath); err != nil {
		log.Printf("Could not open discordConfigPath: '%v', webhook will be disabled", discordConfigPath)
	} else {
		if discordConf, err := skm.LoadDiscordConfig(discordConfigFile); err != nil {
			log.Println("Could not read discord config, webhook will be disabled:", err)
		} else {
			publishHandler.WebhookQueue = discordConf.CreateWebookQueue(&http.Client{})
			log.Println("Using discord webhook")
		}
		discordConfigFile.Close()
	}

	http.Handle("/", skm.Handler{})

	go skm.SaveSessions(sessionPath, &keys)

	http.Handle("/publish", publishHandler)
	http.Handle("/publish_done", skm.PublishDoneHandler{Keys: &keys, Logger: sessionLog, PendingClear: pendingClear})
	http.Handle("/update", skm.UpdateHandler{Keys: &keys})
	http.Handle("/play", skm.PlayHandler{})

	http.Handle("/api/key/", skm.KeyHandler{Keys: &keys, DisconnectQueue: disconnectQueue})

	log.Println("Listening on port", listenPort)
	log.Fatal(http.ListenAndServe("127.0.0.1:"+listenPort, nil))
}
