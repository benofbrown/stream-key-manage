package skm

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

const publishUrl = "http://example.com/publish"
const publishDoneUrl = "http://example.com/publish_done"

func checkLiveKey(key Key) error {
	if key.State != LIVE {
		return fmt.Errorf("Key state not as expected, should be %v, got %v", LIVE, key.State)
	}
	if key.Cooldown != (time.Time{}) {
		return fmt.Errorf("Key Cooldown is %v, should be unset", key.Cooldown)
	}
	if key.PendingCooldown != (time.Time{}) {
		return fmt.Errorf("Key PendingCooldown is %v, should be unset", key.PendingCooldown)
	}
	if key.LiveSince.IsZero() {
		return errors.New("Key LiveSince is not set")
	}
	now := time.Now()
	if !key.LiveSince.Equal(now) && !key.LiveSince.Before(now) {
		return fmt.Errorf("Key LiveSince is %v, should be before or equal %v", key.LiveSince, now)
	}

	return nil
}

func TestPublishBadMethods(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	methods := []string{"GET", "PUT", "HEAD", "OPTIONS", "CONNECT"}
	for _, method := range methods {
		r := httptest.NewRequest(method, publishUrl, nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusMethodNotAllowed {
			t.Fatalf("%s: should be %v, got %v", method, http.StatusMethodNotAllowed, res.StatusCode)
		}
	}
}

func TestPublishBadForm(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	// empty form
	r := httptest.NewRequest("POST", publishUrl, nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Bad form: should be %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	// 'bad' form
	form := strings.NewReader("x")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Bad form: should be %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	// 'bad' form, short read
	form = strings.NewReader("name=abcd")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	r.Body = http.MaxBytesReader(w, r.Body, 1)
	h.ServeHTTP(w, r)

	res = w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Bad form: should be %v, got %v", http.StatusBadRequest, res.StatusCode)
	}
}

func TestPublishMissingKey(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Publish missing key: should be %v, got %v", http.StatusNotFound, res.StatusCode)
	}
}

func TestPublishNonActiveKey(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	h.Keys.StoreKey("abcd", Key{})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Wrong status, expected %v got %v", http.StatusOK, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.ID != "abcd" {
			t.Fatalf("Key contains ID %s, should be 'abcd'", foundKey.ID)
		}
		if err := checkLiveKey(foundKey); err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Key not found after publish")
	}
}

func TestPublishNonActiveKeyOtherActiveEqualPriority(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	h.Keys.StoreKey("abcd", Key{})
	h.Keys.StoreKey("defg", Key{State: LIVE})
	h.Keys.StoreKey("hijk", Key{})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Expected %v, got %v", http.StatusLocked, res.StatusCode)
	}
}

func TestPublishNonActiveKeyOtherActiveHigherPriority(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	h.Keys.StoreKey("abcd", Key{Priority: 11})
	h.Keys.StoreKey("defg", Key{Priority: 10, State: LIVE})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Expected %v, got %v", http.StatusLocked, res.StatusCode)
	}

	select {
	case id := <-queue:
		if id != "defg" {
			t.Fatalf("Incorrect ID in disconnect queue. Wanted defg, got %v", id)
		}
	default:
		t.Fatalf("Nothing in disconnect queue")
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != PENDING_START {
			t.Fatalf("Incorrect state, should be %v, got %v", PENDING_START, foundKey.State)
		}
		if !foundKey.PendingCooldown.After(time.Now()) {
			t.Fatalf("Incorrect pending cooldown, should be after %v, got %v", time.Now(), foundKey.PendingCooldown)
		}
		if foundKey.Cooldown != (time.Time{}) {
			t.Fatalf("Incorrect cooldown, should be %v, got %v", time.Time{}, foundKey.Cooldown)
		}
	} else {
		t.Fatal("abcd not found")
	}

	if foundKey, ok := h.Keys.GetKey("defg"); ok {
		if foundKey.State != PENDING_DISCONNECT {
			t.Fatalf("Incorrect state, should be %v, got %v", PENDING_DISCONNECT, foundKey.State)
		}
		if !foundKey.Cooldown.After(time.Now()) {
			t.Fatalf("Incorrect cooldown, should be after %v, got %v", time.Now(), foundKey.Cooldown)
		}
	} else {
		t.Fatal("defg not found")
	}

	form = strings.NewReader("name=abcd")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)
	h.Keys.StoreKey("abcd", Key{State: PENDING_START, Priority: 10})
	h.Keys.StoreKey("defg", Key{State: PENDING_DISCONNECT})

	res = w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Expected %v, got %v", http.StatusLocked, res.StatusCode)
	}
}

func TestPublishPendingStartKey(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	h.Keys.StoreKey("abcd", Key{State: PENDING_START, Cooldown: time.Now().Add(time.Minute * 3), PendingCooldown: time.Now().Add(time.Minute * 4)})
	h.Keys.StoreKey("defg", Key{State: IDLE})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Incorrect status code, expected %v got %v", http.StatusLocked, res.StatusCode)
	}

	form = strings.NewReader("name=defg")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Incorrect status code (not pending), expected %v got %v", http.StatusLocked, res.StatusCode)
	}

	h.Keys.StoreKey("abcd", Key{State: PENDING_START})
	form = strings.NewReader("name=abcd")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Incorrect status code (pending expired cooldown), expected %v got %v", http.StatusOK, res.StatusCode)
	}

	if foundData, ok := h.Keys.GetKey("abcd"); ok {
		if err := checkLiveKey(foundData); err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Key not found")
	}

	h.Keys.StoreKey("abcd", Key{State: PENDING_START, PendingCooldown: time.Now().Add(time.Minute * -1)})
	form = strings.NewReader("name=defg")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Incorrect status code (other key, expired pending cooldown), expected %v got %v", http.StatusOK, res.StatusCode)
	}

	if foundData, ok := h.Keys.GetKey("abcd"); ok {
		if foundData.State != IDLE {
			t.Fatalf("Unexpected state, expected %v got %v", IDLE, foundData.State)
		}
		if !foundData.LiveSince.IsZero() {
			t.Fatal("LiveSince should be zero")
		}
	} else {
		t.Fatal("Key not found")
	}

	if foundData, ok := h.Keys.GetKey("defg"); ok {
		if err := checkLiveKey(foundData); err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Key not found")
	}

	h.Keys.StoreKey("abcd", Key{State: PENDING_START, Cooldown: time.Now().Add(time.Minute * -1), PendingCooldown: time.Now().Add(time.Minute * 1)})
	h.Keys.StoreKey("defg", Key{})

	form = strings.NewReader("name=abcd")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Incorrect status code (other key, expired pending cooldown), expected %v got %v", http.StatusOK, res.StatusCode)
	}

	if foundData, ok := h.Keys.GetKey("abcd"); ok {
		if err := checkLiveKey(foundData); err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatal("Key not found")
	}
}

func TestPublishActiveKey(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	h.Keys.StoreKey("abcd", Key{State: LIVE})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Expected %v, got %v", http.StatusOK, res.StatusCode)
	}
}

func TestPublishCooldownKey(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}
	cooldown := time.Now().Add(time.Duration(5 * time.Minute))

	testFunc := func(t *testing.T) {
		form := strings.NewReader("name=abcd")
		r := httptest.NewRequest("POST", publishUrl, form)
		r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()

		if res.StatusCode != http.StatusLocked {
			t.Fatalf("Expected %v, got %v", http.StatusLocked, res.StatusCode)
		}

		if foundKey, ok := h.Keys.GetKey("abcd"); ok {
			if foundKey.State != IDLE {
				t.Fatalf("Incorrect state, should be %v, got %v", IDLE, foundKey.State)
			}
			if foundKey.Cooldown != cooldown {
				t.Fatalf("Incorrect cooldown, should be %v, got %v", cooldown, foundKey.Cooldown)
			}
		} else {
			t.Fatal("Key not found")
		}

	}

	h.Keys.StoreKey("abcd", Key{Priority: 100, Cooldown: cooldown})
	t.Run("No current connection", testFunc)

	h.Keys.StoreKey("efgh", Key{State: LIVE, Priority: 12})
	t.Run("takeover from normal priority", testFunc)

	h.Keys.StoreKey("efgh", Key{State: LIVE, Priority: 1})
	t.Run("takeover from low priority", testFunc)
}

func TestPublishCooldownPendingDisconnectKey(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	h.Keys.StoreKey("abcd", Key{State: PENDING_DISCONNECT, Cooldown: time.Now().Add(time.Duration(5 * time.Minute))})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Expected %v, got %v", http.StatusLocked, res.StatusCode)
	}
}

func TestPublishPendingStartExpiredKeyNotLive(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue}

	h.Keys.StoreKey("abcd", Key{State: PENDING_START, Priority: 500, PendingCooldown: time.Now().Add(time.Duration(-5 * time.Minute))})
	h.Keys.StoreKey("defg", Key{State: LIVE})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Expected %v, got %v", http.StatusLocked, res.StatusCode)
	}

	select {
	case id := <-queue:
		if id != "defg" {
			t.Fatalf("Incorrect ID in disconnect queue. Wanted defg, got %v", id)
		}
	default:
		t.Fatalf("Nothing in disconnect queue")
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != PENDING_START {
			t.Fatalf("Incorrect state, should be %v, got %v", PENDING_START, foundKey.State)
		}
		if time.Now().After(foundKey.PendingCooldown) {
			t.Fatalf("PendingCooldown is in the past")
		}
	} else {
		t.Fatalf("Key not found")
	}

	if foundKey, ok := h.Keys.GetKey("defg"); ok {
		if foundKey.State != PENDING_DISCONNECT {
			t.Fatalf("Old key: Incorrect state, should be %v, got %v", PENDING_DISCONNECT, foundKey.State)
		}
		if time.Now().After(foundKey.Cooldown) {
			t.Fatalf("Old key: Cooldown is in the past")
		}
	} else {
		t.Fatalf("Old key not found")
	}

	h.Keys.StoreKey("defg", Key{State: IDLE})

	form = strings.NewReader("name=abcd")
	r = httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Second connect: Expected %v, got %v", http.StatusOK, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if err := checkLiveKey(foundKey); err != nil {
			t.Fatal(err)
		}
	} else {
		t.Fatalf("Second connect: Key not found")
	}
}

func TestPublishSendWebhook(t *testing.T) {
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	webhookQueue := make(chan bool, 8)
	defer close(webhookQueue)
	h := PublishHandler{Keys: &keys, DisconnectQueue: queue, WebhookQueue: webhookQueue}
	keyID := "normal"

	key := Key{Nick: "test", Priority: 12}
	h.Keys.StoreKey(keyID, key)
	Server.SetLastKeyID("")

	expectedQueued := true
	testFunc := func(t *testing.T) {
		form := strings.NewReader("name=" + keyID)
		r := httptest.NewRequest("POST", publishUrl, form)
		r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()

		if res.StatusCode != http.StatusOK {
			t.Fatalf("Send Webhook: Expected %v, got %v", http.StatusOK, res.StatusCode)
		}

		select {
		case queued := <-webhookQueue:
			if queued != expectedQueued {
				t.Fatalf("Incorrect bool in webhook queue. Wanted '%v', got '%v'", expectedQueued, queued)
			}
		default:
			t.Fatal("Nothing in webhook queue")
		}
	}

	t.Run("Key with Nick", testFunc)

	h.Keys.StoreKey(keyID, Key{Priority: 12})
	t.Run("Key without Nick", testFunc)
	h.Keys.StoreKey(keyID, Key{Priority: 12})

	keyID = "low"
	h.Keys.StoreKey(keyID, Key{Priority: 10})
	t.Run("Low priority key - first connect", testFunc)

	h.Keys.StoreKey(keyID, Key{Priority: 10})
	expectedQueued = false
	t.Run("Low priority key - second connect", testFunc)
	h.Keys.StoreKey(keyID, Key{Priority: 10})

	//different key
	expectedQueued = true
	keyID = "normal2"
	h.Keys.StoreKey(keyID, Key{Priority: 12})
	t.Run("Second key", testFunc)
	h.Keys.StoreKey(keyID, Key{Priority: 12})

	keyID = "zero"
	expectedQueued = false
	h.Keys.StoreKey(keyID, Key{Priority: 0})
	t.Run("Zero priority key", testFunc)
	h.Keys.StoreKey(keyID, Key{Priority: 0})

	keyID = "low"
	expectedQueued = true
	h.Keys.StoreKey(keyID, Key{Priority: 10})
	t.Run("Low priority", testFunc)
	h.Keys.StoreKey(keyID, Key{Priority: 10})

	keyID = "normal"
	h.Keys.StoreKey(keyID, Key{Priority: 12})
	t.Run("Normal priority after low", testFunc)
	h.Keys.StoreKey(keyID, Key{Priority: 12})

	keyID = "low"
	h.Keys.StoreKey(keyID, Key{Priority: 10})
	t.Run("Low priority again", testFunc)
	h.Keys.StoreKey(keyID, Key{Priority: 10})
}

func TestPublishDoneBadMethods(t *testing.T) {
	keys := Keys{}
	h := PublishDoneHandler{Keys: &keys}

	methods := []string{"GET", "PUT", "HEAD", "OPTIONS", "CONNECT"}
	for _, method := range methods {
		r := httptest.NewRequest(method, publishDoneUrl, nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusMethodNotAllowed {
			t.Fatalf("%s: should be %v, got %v", method, http.StatusMethodNotAllowed, res.StatusCode)
		}
	}
}

func TestPublishDoneBadForm(t *testing.T) {
	keys := Keys{}
	h := PublishDoneHandler{Keys: &keys}

	r := httptest.NewRequest("POST", publishDoneUrl, nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Should be %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	r = httptest.NewRequest("POST", publishDoneUrl, strings.NewReader("name=abcd"))
	w = httptest.NewRecorder()
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Body = http.MaxBytesReader(w, r.Body, 1)
	h.ServeHTTP(w, r)

	res = w.Result()
	if res.StatusCode != http.StatusInternalServerError {
		t.Fatalf("Should be %v, got %v", http.StatusInternalServerError, res.StatusCode)
	}
}

func TestPublishDone(t *testing.T) {
	keys := Keys{}
	h := PublishDoneHandler{Keys: &keys}

	h.Keys.StoreKey("abcd", Key{State: LIVE, LiveSince: time.Now().Add(-1 * time.Minute)})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishDoneUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusOK {
		t.Fatalf("Should be %v, got %v", http.StatusOK, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != IDLE {
			t.Fatalf("Incorrect state, should be %v, is %v", IDLE, foundKey.State)
		}
		if !foundKey.LiveSince.IsZero() {
			t.Fatal("LiveSince should be zero")
		}
	} else {
		t.Fatal("Key not found")
	}
}

func TestPublishConstructor(t *testing.T) {
	keys := Keys{}
	disconnect := make(chan string)
	pendingClear := make(chan bool)

	defer close(disconnect)
	defer close(pendingClear)

	h := NewPublishHandler(&keys, disconnect, pendingClear)

	if h.Keys != &keys {
		t.Fatal("Keys didn't match")
	}

	if h.DisconnectQueue != disconnect {
		t.Fatal("disconnect doesn't match")
	}

	if h.PendingClear != pendingClear {
		t.Fatal("pendingClear doesn't match")
	}

	if h.ConnectCooldownTime != ConnectPendingCooldownTime {
		t.Fatal("ConnectCooldownTime time doesn't match")
	}
}

func TestPublishClearOK(t *testing.T) {
	keys := Keys{}
	disconnectQueue := make(chan string, 2)
	pendingClear := make(chan bool, 2)

	defer close(disconnectQueue)
	defer close(pendingClear)

	keyData := Key{ID: "abcd"}
	keys.StoreKey("abcd", keyData)
	activeKey := Key{ID: "efgh", State: LIVE}
	keys.StoreKey("efgh", activeKey)

	h := PublishHandler{Keys: &keys, DisconnectQueue: disconnectQueue, PendingClear: pendingClear}
	h.ConnectCooldownTime = 100 * time.Millisecond

	go func() {
		time.Sleep(20 * time.Millisecond)
		d := PublishDoneHandler{Keys: &keys, PendingClear: pendingClear}
		form := strings.NewReader("name=efgh")
		r := httptest.NewRequest("POST", publishDoneUrl, form)
		r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		w := httptest.NewRecorder()

		d.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusOK {
			log.Fatalf("Unexpected status, wanted %v but got %v", http.StatusOK, res.StatusCode)
		}
	}()

	w := httptest.NewRecorder()
	h.takeoverKey("abcd", activeKey, keyData, w)

	res := w.Result()
	if res.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected status, wanted %v but got %v", http.StatusOK, res.StatusCode)
	}

	if key, ok := keys.GetKey("abcd"); ok {
		if key.State != LIVE {
			t.Fatalf("Key should be %v, is %v", LIVE, key.State)
		}
	} else {
		t.Fatal("Key not found")
	}

	if key, ok := keys.GetKey("efgh"); ok {
		if key.State != IDLE {
			t.Fatalf("Key should be %v, is %v", IDLE, key.State)
		}
	} else {
		t.Fatal("Old key not found")
	}

}

func TestPublishClearTimeout(t *testing.T) {
	keys := Keys{}
	disconnectQueue := make(chan string, 2)
	pendingClear := make(chan bool, 2)

	defer close(disconnectQueue)
	defer close(pendingClear)

	keyData := Key{ID: "abcd"}
	keys.StoreKey("abcd", keyData)
	activeKey := Key{ID: "efgh", State: LIVE}
	keys.StoreKey("efgh", activeKey)

	h := PublishHandler{Keys: &keys, DisconnectQueue: disconnectQueue, PendingClear: pendingClear}
	h.ConnectCooldownTime = 100 * time.Millisecond

	w := httptest.NewRecorder()
	h.takeoverKey("abcd", activeKey, keyData, w)

	res := w.Result()
	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Unexpected status, wanted %v but got %v", http.StatusLocked, res.StatusCode)
	}

	if key, ok := keys.GetKey("abcd"); ok {
		if key.State != PENDING_START {
			t.Fatalf("Key should be %v, is %v", PENDING_START, key.State)
		}
	} else {
		t.Fatal("Key not found")
	}

	if key, ok := keys.GetKey("efgh"); ok {
		if key.State != PENDING_DISCONNECT {
			t.Fatalf("Key should be %v, is %v", PENDING_DISCONNECT, key.State)
		}
	} else {
		t.Fatal("Old key not found")
	}

}

func TestPublishStreamtrain(t *testing.T) {
	keys := Keys{}
	disconnect := make(chan string, 30)
	defer close(disconnect)
	pendingClear := make(chan bool, 8)
	defer close(pendingClear)

	keys.StoreKey("abcd", Key{})
	h := NewPublishHandler(&keys, disconnect, pendingClear)
	d := PublishDoneHandler{Keys: &keys, PendingClear: pendingClear}

	var handler http.Handler
	var i int

	testFunc := func(t *testing.T) {
		t.Logf("Attempt %d", i)
		form := strings.NewReader("name=abcd")
		r := httptest.NewRequest("POST", publishUrl, form)
		r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		w := httptest.NewRecorder()
		handler.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusOK {
			t.Fatalf("Unexpected status code, wanted %v got %v", http.StatusOK, res.StatusCode)
		}
	}

	for i = 0; i < 10; i++ {
		handler = h
		t.Run(fmt.Sprintf("PublishAttempt%d", i), testFunc)
		handler = d
		t.Run(fmt.Sprintf("PublishDoneAttempt%d", i), testFunc)
	}
}

func TestPublishActiveKeyPendingDisconnect(t *testing.T) {
	keys := Keys{}

	current := Key{State: PENDING_DISCONNECT, Cooldown: time.Now().Add(time.Minute), PendingCooldown: time.Now().Add(DisconnectPendingCooldownTime)}

	keys.StoreKey("new", Key{})
	keys.StoreKey("current", current)
	h := PublishHandler{Keys: &keys}

	expectedCode := http.StatusLocked

	testFunc := func(t *testing.T) {
		form := strings.NewReader("name=new")
		r := httptest.NewRequest("POST", publishUrl, form)
		r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != expectedCode {
			t.Fatalf("Unexpected status code, wanted %v got %v", expectedCode, res.StatusCode)
		}
	}

	t.Run("ConnectPrePendingCooldownExpiry", testFunc)

	current.PendingCooldown = time.Now().Add(-1 * time.Minute)
	keys.StoreKey("current", current)
	expectedCode = http.StatusOK

	t.Run("ConnectPostPendingCooldownExpiry", testFunc)
}

func TestDoneLog(t *testing.T) {
	keys := Keys{}
	buf := bytes.Buffer{}
	log := Logger{logger: log.New(&buf, "", 0)}

	h := PublishDoneHandler{Keys: &keys, Logger: &log}
	keys.StoreKey("abcd", Key{Nick: "abcd's Nick", State: LIVE})
	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", publishUrl, form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	if !strings.Contains(buf.String(), "Session ended for abcd (abcd's Nick), ran from") {
		t.Fatalf("Log message not as expected. Message follows: %v", buf.String())
	}
}
