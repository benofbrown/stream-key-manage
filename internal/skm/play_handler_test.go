package skm

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestPlayBadMethods(t *testing.T) {
	h := PlayHandler{}

	methods := []string{"GET", "PUT", "HEAD", "OPTIONS", "CONNECT"}
	for _, method := range methods {
		r := httptest.NewRequest(method, "http://example.com/play", nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusMethodNotAllowed {
			t.Fatalf("%s: should be %v, got %v", method, http.StatusMethodNotAllowed, res.StatusCode)
		}
	}
}

func TestPlayBadForm(t *testing.T) {
	h := PlayHandler{}

	form := strings.NewReader("app=&flashver=ngx-local-relay")
	r := httptest.NewRequest("POST", "http://example.com/play", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	r.Body = http.MaxBytesReader(w, r.Body, 1)
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}
}

func TestPlayOnPush(t *testing.T) {
	h := PlayHandler{}

	form := strings.NewReader("app=&flashver=ngx-local-relay")
	r := httptest.NewRequest("POST", "http://example.com/play", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}
}

func TestBarePlay(t *testing.T) {
	h := PlayHandler{}

	form := strings.NewReader("app=live&flashver=LNX 9,0,124,2")
	r := httptest.NewRequest("POST", "http://example.com/play", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusForbidden {
		t.Fatalf("Expected %v, got %v", http.StatusForbidden, res.StatusCode)
	}
}
