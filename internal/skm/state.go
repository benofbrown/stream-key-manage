package skm

import (
	"bytes"
)

const (
	IDLE    = 0
	UNKNOWN = 1

	/*
	   We can use bitwise operations to consider both PENDING_DISCONNECT and LIVE
	   as active states. Additionally PENDING can apply to both PENDING_START and
	   PENDING_DISCONNECT but PENDING_START isn't considered ACTIVE.
	*/
	PENDING = 2
	ACTIVE  = 4

	LIVE = 5

	PENDING_START      = 3
	PENDING_DISCONNECT = 6
)

type State int

func (s State) String() string {
	switch s {
	case IDLE:
		return "IDLE"
	case PENDING_START:
		return "PENDING_START"
	case LIVE:
		return "LIVE"
	case PENDING_DISCONNECT:
		return "PENDING_DISCONNECT"
	default:
		return "UNKNOWN"
	}
}

func (s State) MarshalJSON() ([]byte, error) {
	var b bytes.Buffer
	b.Grow(20)
	b.WriteString("\"")
	b.WriteString(s.String())
	b.WriteString("\"")
	return b.Bytes(), nil
}

func (s *State) UnmarshalJSON(b []byte) error {
	switch string(bytes.Trim(b, "\"")) {
	case "IDLE":
		*s = IDLE
	case "PENDING_START":
		*s = PENDING_START
	case "LIVE":
		*s = LIVE
	case "PENDING_DISCONNECT":
		*s = PENDING_DISCONNECT
	default:
		*s = UNKNOWN
	}

	return nil
}
