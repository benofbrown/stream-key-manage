package skm

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMethods(t *testing.T) {
	h := Handler{}

	methods := []string{"GET", "POST", "DELETE", "HEAD", "OPTIONS", "CONNECT"}

	for _, method := range methods {
		r := httptest.NewRequest(method, "http://example.com/api/", nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusNotFound {
			t.Fatalf("%s: Expected %v got %v", method, http.StatusNotFound, res.StatusCode)
		}
	}
}
