package skm

import (
	"log"
	"time"

	"gopkg.in/natefinch/lumberjack.v2"
)

type Logger struct {
	logger *log.Logger
	lj     lumberjack.Logger
}

func LogSetup(path string) *Logger {
	l := Logger{}
	l.lj = lumberjack.Logger{Filename: path}
	l.logger = log.New(&l.lj, "", log.Ldate|log.Ltime|log.LUTC)
	return &l
}

func (l *Logger) LogSession(key Key) {
	if l.logger == nil {
		return
	}

	tf := "2006-01-02 15:04:05"

	l.logger.Printf("Session ended for %v (%v), ran from %s to %s", key.ID, key.Nick, key.LiveSince.UTC().Format(tf), time.Now().UTC().Format(tf))
}

func (l *Logger) Rotate() {
	if l.lj == (lumberjack.Logger{}) {
		return
	}

	if err := l.lj.Rotate(); err != nil {
		log.Println("Could not rotate log")
	}
}
