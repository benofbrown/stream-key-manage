package skm

import (
	"bytes"
	"strconv"
)

type Snowflake uint64

func (s Snowflake) String() string {
	return strconv.FormatUint(uint64(s), 10)
}

func (s Snowflake) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}
	b.WriteString("\"")
	b.WriteString(s.String())
	b.WriteString("\"")
	return b.Bytes(), nil
}

func (s *Snowflake) UnmarshalJSON(b []byte) error {
	str := string(bytes.Trim(b, "\""))
	intval, err := strconv.ParseUint(str, 10, 64)
	*s = Snowflake(intval)
	return err
}
