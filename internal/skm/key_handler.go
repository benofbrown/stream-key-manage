package skm

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type KeyHandler struct {
	Keys            *Keys
	DisconnectQueue chan string
}

type disconnectRequest struct {
	Cooldown string `json:"cooldown"`
	Force    bool   `json:"force"`
}

func splitURL(url *url.URL) (string, string) {
	parts := strings.Split(url.Path, "/")
	if len(parts) < 4 {
		return "", ""
	}

	if len(parts) < 5 {
		return parts[3], ""
	} else {
		return parts[3], parts[4]
	}
}

func (h KeyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost && r.Method != http.MethodPut && r.Method != http.MethodGet && r.Method != http.MethodDelete {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if r.Method == http.MethodPost {
		if id, _ := splitURL(r.URL); id != "" {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		key := Key{}
		dec := json.NewDecoder(r.Body)
		if err := dec.Decode(&key); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "Failed to parse body as JSON:", err)
			return
		}

		if key.ID != "" {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "Requests for new keys must not have an ID set")
			return
		}

		if key.State != IDLE {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "Requests for new keys must not have the State set")
			return
		}

		if key.Nick == "" {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, "Requests for new keys must have the Nick set")
			return
		}

		id := uuid.New().String()
		h.Keys.StoreKey(id, key)

		key.ID = id
		log.Println("Added key", key)
		SendJSONAccepted(w, key)
		return
	}

	if r.URL.Path == "/api/key/" && r.Method == http.MethodGet {
		queryVals := r.URL.Query()
		if nick := queryVals.Get("nick"); nick != "" {
			if keys, ok := h.Keys.GetKeysByNick(nick); ok {
				SendJSON(w, keys, true)
				return
			} else {
				w.WriteHeader(http.StatusNotFound)
				return
			}
		}
		if discordID := queryVals.Get("discord_id"); discordID != "" {
			if intval, err := strconv.ParseUint(discordID, 10, 64); err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			} else {
				if keys, ok := h.Keys.GetKeysByDiscordID(Snowflake(intval)); ok {
					SendJSON(w, keys, false)
					return
				} else {
					w.WriteHeader(http.StatusNotFound)
					return
				}
			}
		}
		keys := []Key{}
		h.Keys.Range(func(k, v interface{}) bool {
			keyData := v.(Key)
			keyData.ID = k.(string)
			keys = append(keys, keyData)
			return true
		})
		SendJSON(w, keys, true)
		return
	}

	id, command := splitURL(r.URL)
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if command == "disconnect" {
		if r.Method != http.MethodPut && r.Method != http.MethodDelete {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		if id == "active" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		requestKey := disconnectRequest{}
		cooldownTime := time.Time{}
		force := false
		if r.ContentLength != 0 {
			dec := json.NewDecoder(r.Body)
			if err := dec.Decode(&requestKey); err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			if requestKey.Cooldown != "" {
				dur, err := time.ParseDuration(requestKey.Cooldown)
				if err != nil {
					w.WriteHeader(http.StatusBadRequest)
					return
				}
				cooldownTime = time.Now().Add(dur)
			}

			if requestKey.Force {
				force = true
			}
		}

		if r.Method == http.MethodPut {
			err := h.Keys.DisconnectKey(id, h.DisconnectQueue, cooldownTime, force)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
				return
			}

			w.WriteHeader(http.StatusAccepted)
			return
		}

		if r.Method == http.MethodDelete {
			err := h.Keys.DisconnectKey(id, h.DisconnectQueue, cooldownTime, force)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			h.Keys.Delete(id)
			w.WriteHeader(http.StatusAccepted)
			return
		}
	} else if command == "reset" {
		if r.Method != http.MethodPut {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		if id == "active" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if foundKey, ok := h.Keys.GetKey(id); ok {
			if foundKey.State&ACTIVE > 0 {
				w.WriteHeader(http.StatusLocked)
				return
			}

			foundKey.Cooldown = time.Time{}
			foundKey.PendingCooldown = time.Time{}
			foundKey.State = IDLE
			h.Keys.StoreKey(id, foundKey)
			w.WriteHeader(http.StatusAccepted)
			return
		} else {
			w.WriteHeader(http.StatusNotFound)
			return
		}

	}

	if id == "active" {
		if keyData, ok := h.Keys.GetActiveKey(); ok {
			if r.Method == http.MethodGet {
				SendJSON(w, keyData, false)
				return
			} else {
				w.WriteHeader(http.StatusMethodNotAllowed)
				return
			}
		} else {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	}

	if keyData, ok := h.Keys.GetKey(id); !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	} else {
		if r.Method == http.MethodGet {
			SendJSON(w, keyData, false)
			return
		} else if r.Method == http.MethodPut {
			newData := Key{}
			decoder := json.NewDecoder(r.Body)
			if err := decoder.Decode(&newData); err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			if newData == (Key{}) {
				SendJSONAccepted(w, keyData)
				return
			}

			if newData.ID != "" {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			if newData.Priority > 0 {
				keyData.Priority = newData.Priority
			}

			if newData.Nick != "" {
				keyData.Nick = newData.Nick
			}

			if newData.Cooldown != (time.Time{}) {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			if newData.PendingCooldown != (time.Time{}) {
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			if newData.DiscordID != 0 {
				// A valid ID will have at least one of the first 41 bits set.
				// We use an invalid ID here to remove the DiscordID from a key.
				if newData.DiscordID&0xFFFFFFFFFF800000 > 0 {
					keyData.DiscordID = newData.DiscordID
				} else {
					keyData.DiscordID = 0
				}
			}

			h.Keys.StoreKey(id, keyData)
			log.Println("Updated key", keyData)

			SendJSONAccepted(w, keyData)
			return
		} else if r.Method == http.MethodDelete {
			if keyData.State&ACTIVE > 0 {
				w.WriteHeader(http.StatusLocked)
				return
			}

			h.Keys.Delete(keyData.ID)
			w.WriteHeader(http.StatusAccepted)
			return
		}
	}
}
