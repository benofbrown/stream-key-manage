package skm

import (
	"log"
	"net/http"
	"strconv"
	"time"
)

type UpdateHandler struct {
	Keys *Keys
}

func (h UpdateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if key := r.Form.Get("name"); key != "" {
		if updatedKeyData, ok := h.Keys.GetKey(key); ok {
			if updatedKeyData.State == PENDING_DISCONNECT {
				updatedKeyData.State = IDLE
				updatedKeyData.PendingCooldown = time.Time{}
				h.Keys.StoreKey(key, updatedKeyData)
				w.WriteHeader(http.StatusForbidden)
				LogRequest(r)
				log.Printf("Dropping connection %s", key)
				return
			} else if updatedKeyData.State == IDLE {
				LogRequest(r)
				if Server.GetActive() {
					log.Printf("Already auto-enabled a connection in this session, dropping %s", key)
					w.WriteHeader(http.StatusLocked)
					return
				}
				log.Printf("Changing %s to LIVE", key)
				updatedKeyData.State = LIVE
				if liveSeconds := r.Form.Get("time"); liveSeconds != "" {
					if intLiveSeconds, err := strconv.Atoi(liveSeconds); err == nil {
						updatedKeyData.LiveSince = time.Now().Add(time.Duration(intLiveSeconds) * time.Second * -1)
					}
				}
				h.Keys.StoreKey(key, updatedKeyData)
				Server.SetActive()
			}
		} else {
			LogRequest(r)
			log.Printf("Key %s not found - dropping connection", key)
			w.WriteHeader(http.StatusNotFound)
			return
		}
	} else {
		log.Printf("Bad request, 'name' missing")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}
