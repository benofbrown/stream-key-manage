package skm

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
)

func SendJSON(w http.ResponseWriter, data interface{}, indent bool) {
	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	if indent {
		encoder.SetIndent("", "  ")
	}
	if err := encoder.Encode(data); err != nil {
		log.Printf("ERROR: could not encode to JSON: %v", err)
	}
}

func SendJSONAccepted(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	encoder := json.NewEncoder(w)
	if err := encoder.Encode(data); err != nil {
		log.Println("ERROR: could not encode to JSON:", err)
	}
}

func LogRequest(r *http.Request) {
	qsep := "?"
	if r.URL.RawQuery == "" {
		qsep = ""
	}

	body := strings.Builder{}
	if (r.Method == http.MethodPost || r.Method == http.MethodPut) && r.ContentLength != 0 {
		if err := r.ParseForm(); err != nil {
			log.Println("Could not parse form")
		} else {
			sep := ""
			keys := []string{}
			for k := range r.PostForm {
				keys = append(keys, k)
			}
			sort.Strings(keys)

			for _, k := range keys {
				body.WriteString(fmt.Sprintf("%s%s=%s", sep, k, r.PostForm[k]))
				sep = " "
			}
		}
	}

	log.Printf(`%s %s "%s%s%s" "%s"`, r.Header.Get("X-SKM-Server"), r.Method, r.URL.Path, qsep, r.URL.RawQuery, body.String())
}
