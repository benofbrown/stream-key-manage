package skm

import (
	"bytes"
	"encoding/json"
	"errors"
	"log"
	"strings"
	"sync"
	"time"
)

type Key struct {
	ID              string    `json:"id,omitempty"`
	Cooldown        time.Time `json:"cooldown"`
	Priority        int       `json:"priority"`
	State           State     `json:"state"`
	Nick            string    `json:"nick"`
	PendingCooldown time.Time `json:"pending_cooldown"`
	DiscordID       Snowflake `json:"discord_id,omitempty"`
	LiveSince       time.Time `json:"live_since"`
}

func (k Key) String() string {
	buf := bytes.Buffer{}
	enc := json.NewEncoder(&buf)
	if err := enc.Encode(k); err != nil {
		return "<INVALID>"
	}
	return strings.Trim(buf.String(), "\n")
}

type Keys struct {
	sync.Map
}

func (k *Keys) GetActiveKey() (Key, bool) {
	key := Key{}
	ok := false
	k.Range(func(k, v interface{}) bool {
		key = v.(Key)
		if key.State&ACTIVE > 0 {
			key.ID = k.(string)
			ok = true
			return false
		}
		return true
	})
	return key, ok
}

func (k *Keys) GetPendingKey() (Key, bool) {
	key := Key{}
	ok := false
	k.Range(func(k, v interface{}) bool {
		key = v.(Key)
		if key.State&PENDING > 0 {
			key.ID = k.(string)
			ok = true
			return false
		}
		return true
	})
	return key, ok
}

func (k *Keys) GetKey(id string) (Key, bool) {
	if key, ok := k.Load(id); ok {
		keyData := key.(Key)
		keyData.ID = id
		return keyData, ok
	} else {
		return Key{}, false
	}
}

func (k *Keys) GetKeysByDiscordID(discordID Snowflake) ([]Key, bool) {
	keys := []Key{}
	ok := false
	k.Range(func(k, v interface{}) bool {
		key := v.(Key)
		if key.DiscordID == discordID {
			key.ID = k.(string)
			ok = true
			keys = append(keys, key)
		}
		return true
	})
	return keys, ok
}

func (k *Keys) GetKeysByNick(nick string) ([]Key, bool) {
	keys := []Key{}
	ok := false
	nick = strings.ToLower(nick)
	k.Range(func(k, v interface{}) bool {
		key := v.(Key)
		if strings.ToLower(key.Nick) == nick {
			key.ID = k.(string)
			ok = true
			keys = append(keys, key)
		}
		return true
	})
	return keys, ok
}

func (k *Keys) StoreKey(id string, key Key) {
	log.Println("Storing", id, key)
	key.ID = ""
	k.Store(id, key)
	Server.SetSaveRequired()
}

func (k *Keys) DisconnectKey(id string, disconnectQueue chan string, cooldown time.Time, force bool) error {
	if keyData, ok := k.GetKey(id); ok {
		if keyData.State&ACTIVE > 0 {
			if force {
				keyData.State = IDLE
			} else {
				keyData.State = PENDING_DISCONNECT
			}
			keyData.Cooldown = cooldown
			keyData.PendingCooldown = time.Now().Add(DisconnectPendingCooldownTime)
			k.StoreKey(id, keyData)
			if disconnectQueue != nil {
				disconnectQueue <- id
			}
		}
		return nil
	}
	return errors.New("Not Found")
}
