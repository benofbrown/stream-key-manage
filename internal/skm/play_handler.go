package skm

import (
	"net/http"
)

type PlayHandler struct{}

func (h PlayHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	app := r.PostForm.Get("app")
	flashver := r.PostForm.Get("flashver")

	if app == "" && flashver == "ngx-local-relay" {
		w.WriteHeader(http.StatusAccepted)
		return
	}

	w.WriteHeader(http.StatusForbidden)
}
