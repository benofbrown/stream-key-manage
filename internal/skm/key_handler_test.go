package skm

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestKeyHandlerBadMethods(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{})

	methods := []string{"OPTIONS", "CONNECT"}

	for _, method := range methods {
		r := httptest.NewRequest(method, "http://example.com/api/key/abcd", nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()

		if res.StatusCode != http.StatusMethodNotAllowed {
			t.Fatalf("Wrong status code for %s, expected %v, got %v", method, http.StatusMethodNotAllowed, res.StatusCode)
		}
	}
}

func TestKeyHandlerKeyList(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{})

	r := httptest.NewRequest("GET", "http://example.com/api/key/", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusOK, res.StatusCode)
	}

	keyList := []Key{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&keyList); err != nil {
		t.Fatal("Couldn't decode response body")
	}
}

func TestKeyHandlerGetNotFound(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	r := httptest.NewRequest("GET", "http://example.com/api/key/abcd", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusNotFound, res.StatusCode)
	}
}

func TestKeyHandlerGetFound(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	testKey := Key{Nick: "Test"}
	h.Keys.StoreKey("abcd", testKey)

	r := httptest.NewRequest("GET", "http://example.com/api/key/abcd", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusOK, res.StatusCode)
	}

	if res.Header.Get("Content-Type") != "application/json" {
		t.Fatalf("Wrong content-type, expected application/json, got %v", res.Header.Get("Content-Type"))
	}

	returnedKey := Key{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&returnedKey); err != nil {
		t.Fatal(err)
	}

	testKey.ID = "abcd"
	if testKey != returnedKey {
		t.Fatalf("Wrong key returned, expected %v, got %v", testKey, returnedKey)
	}
}

func TestKeyHandlerGetActive(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	testKey := Key{Nick: "Test"}
	h.Keys.StoreKey("abcd", testKey)

	r := httptest.NewRequest("GET", "http://example.com/api/key/active", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusNotFound, res.StatusCode)
	}

	testKey.State = LIVE
	h.Keys.StoreKey("abcd", testKey)

	r = httptest.NewRequest("GET", "http://example.com/api/key/active", nil)
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusOK, res.StatusCode)
	}

	if res.Header.Get("Content-Type") != "application/json" {
		t.Fatalf("Wrong content-type, expected application/json, got %v", res.Header.Get("Content-Type"))
	}

	returnedKey := Key{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&returnedKey); err != nil {
		t.Fatal(err)
	}

	testKey.ID = "abcd"
	if testKey != returnedKey {
		t.Fatalf("Wrong key returned, expected %v, got %v", testKey, returnedKey)
	}
}

func TestKeyHandlerGetByDiscordNotFound(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	h.Keys.Store("abcd", Key{})
	r := httptest.NewRequest("GET", "http://example.com/api/key/?discord_id=12345678", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusNotFound, res.StatusCode)
	}

	body := strings.Builder{}
	if n, err := io.Copy(&body, res.Body); err != nil {
		t.Fatal(err)
	} else {
		if n > 0 {
			t.Fatalf("Body was not empty, was %s", body.String())
		}
	}

}

func TestKeyHandlerGetByDiscordBadId(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	r := httptest.NewRequest("GET", "http://example.com/api/key/?discord_id=abcd", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}
}

func TestKeyHandlerGetByDiscord(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	testKey := Key{DiscordID: 12345678}
	h.Keys.StoreKey("abcd", testKey)
	h.Keys.StoreKey("defg", Key{DiscordID: 91011121})

	r := httptest.NewRequest("GET", "http://example.com/api/key/?discord_id=12345678", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusOK, res.StatusCode)
	}

	returnedKeys := []Key{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&returnedKeys); err != nil {
		t.Fatal(err)
	}

	if len(returnedKeys) < 1 {
		t.Fatal("Not enough keys returned")
	}

	testKey.ID = "abcd"
	if returnedKeys[0] != testKey {
		t.Fatalf("Incorrect key returned, expected %v, got %v", testKey, returnedKeys[0])
	}
}

func TestKeyHandlerGetByDiscordMultiple(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{DiscordID: 12345678})
	h.Keys.StoreKey("defg", Key{DiscordID: 91011121})
	h.Keys.StoreKey("ghij", Key{DiscordID: 12345678})

	r := httptest.NewRequest("GET", "http://example.com/api/key/?discord_id=12345678", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusOK, res.StatusCode)
	}

	returnedKeys := []Key{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&returnedKeys); err != nil {
		t.Fatal(err)
	}

	if len(returnedKeys) < 2 {
		t.Fatal("Not enough keys returned")
	}

	for _, key := range returnedKeys {
		if keyData, ok := h.Keys.GetKey(key.ID); ok {
			if keyData != key {
				t.Fatalf("Key mismatch, returned was %v, stored was %v", key, keyData)
			}
		} else {
			t.Fatalf("Could not find stored key for %v", key)
		}
	}
}

func TestKeyHandlerGetByNick(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	testKey := Key{Nick: "test1"}
	h.Keys.StoreKey("abcd", testKey)
	h.Keys.StoreKey("defg", Key{Nick: "test2"})

	testNick := "test1"
	expectedStatus := http.StatusOK
	expectBody := true

	testFunc := func(t *testing.T) {
		url := fmt.Sprintf("http://example.com/api/key/?nick=%s", testNick)
		r := httptest.NewRequest("GET", url, nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()

		if res.StatusCode != expectedStatus {
			t.Fatalf("Wrong status code, expected %v, got %v", expectedStatus, res.StatusCode)
		}

		if !expectBody {
			return
		}

		returnedKeys := []Key{}
		dec := json.NewDecoder(res.Body)
		if err := dec.Decode(&returnedKeys); err != nil {
			t.Fatal(err)
		}

		if len(returnedKeys) != 1 {
			t.Fatal("Wrong amount of keys returned")
		}

		testKey.ID = "abcd"
		if returnedKeys[0] != testKey {
			t.Fatalf("Incorrect key returned, expected %v, got %v", testKey, returnedKeys[0])
		}
	}

	t.Run("MatchingCase", testFunc)
	testNick = "TEST1"
	t.Run("DiffernetCase", testFunc)
	testNick = "invalid"
	expectedStatus = http.StatusNotFound
	expectBody = false
	t.Run("UnknownNick", testFunc)
}

func TestKeyHandlerGetByNickMultiple(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{Nick: "test1"})
	h.Keys.StoreKey("defg", Key{Nick: "test2"})
	h.Keys.StoreKey("ghij", Key{Nick: "test1"})

	r := httptest.NewRequest("GET", "http://example.com/api/key/?nick=test1", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusOK {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusOK, res.StatusCode)
	}

	returnedKeys := []Key{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&returnedKeys); err != nil {
		t.Fatal(err)
	}

	if len(returnedKeys) < 2 {
		t.Fatal("Not enough keys returned")
	}

	for _, key := range returnedKeys {
		if key.Nick != "test1" {
			t.Fatalf("Nick mismatch, expected test1, got %v", key.Nick)
		}
		if keyData, ok := h.Keys.GetKey(key.ID); ok {
			if keyData != key {
				t.Fatalf("Key mismatch, returned was %v, stored was %v", key, keyData)
			}
		} else {
			t.Fatalf("Could not find stored key for %v", key)
		}
	}
}

func TestKeyHandlerPost(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	testBodies := []string{
		"",
		"{}",
		`{"id":"abcd"}`,
		`{"state":"LIVE"}`,
		`{"nick":""}`,
	}

	for _, body := range testBodies {
		r := httptest.NewRequest("POST", "http://example.com/api/key", io.NopCloser(strings.NewReader(body)))
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusBadRequest {
			t.Fatalf("Wrong status, expected %v, got %v. Body was %s", http.StatusBadRequest, res.StatusCode, body)
		}
	}

	r := httptest.NewRequest("POST", "http://example.com/api/key", io.NopCloser(strings.NewReader(`{"nick":"Test","discord_id":"1234567890"}`)))
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Wrong status, expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	contentType := res.Header.Get("Content-Type")
	if contentType != "application/json" {
		t.Fatalf("Incorrect content type, should be application/json, got %v", contentType)
	}

	returnedKey := Key{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&returnedKey); err != nil {
		t.Fatal("Key could not be decoded")
	}

	if returnedKey.ID == "" {
		t.Fatal("ID not set in returned key")
	}

	if returnedKey.DiscordID == 0 {
		t.Fatal("DiscordID not set in returned key")
	} else if returnedKey.DiscordID != 1234567890 {
		t.Fatalf("DiscordID incorrect in returned key, expected %v, got %v", 1234567890, returnedKey.DiscordID)
	}

	if _, ok := h.Keys.GetKey(returnedKey.ID); !ok {
		t.Fatal("Could not find returned key in the key store")
	}
}

func TestKeyHandlerPut(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	r := httptest.NewRequest("PUT", "http://example.com/api/key", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Wrong status, expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	r = httptest.NewRequest("PUT", "http://example.com/api/key/abcd", nil)
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()
	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Wrong status, expected %v, got %v", http.StatusNotFound, res.StatusCode)
	}

	h.Keys.StoreKey("abcd", Key{})

	type expectation struct {
		body        string
		status      int
		key         Key
		contentType string
	}

	appJson := "application/json"

	expectations := []expectation{
		{body: "", status: http.StatusBadRequest, key: Key{ID: "abcd"}},
		{body: "{}", status: http.StatusAccepted, key: Key{ID: "abcd"}, contentType: appJson},
		{body: `{"id":"abcd"}`, status: http.StatusBadRequest, key: Key{ID: "abcd"}},
		{body: `{"priority":100}`, status: http.StatusAccepted, key: Key{ID: "abcd", Priority: 100}, contentType: appJson},
		{body: `{"nick":"test"}`, status: http.StatusAccepted, key: Key{ID: "abcd", Priority: 100, Nick: "test"}, contentType: appJson},
		{body: `{"state":LIVE}`, status: http.StatusBadRequest, key: Key{ID: "abcd", Priority: 100, Nick: "test"}},
		{body: `{"state":LIVE}`, status: http.StatusBadRequest, key: Key{ID: "abcd", Priority: 100, Nick: "test"}},
		{body: `{"cooldown":"2020-10-10T01:23:45Z"}`, status: http.StatusBadRequest, key: Key{ID: "abcd", Priority: 100, Nick: "test"}},
		{body: `{"pending_cooldown":"2020-10-10T01:23:45Z"}`, status: http.StatusBadRequest, key: Key{ID: "abcd", Priority: 100, Nick: "test"}},
		{body: `{"discord_id":"1234567890"}`, status: http.StatusAccepted, key: Key{ID: "abcd", Priority: 100, Nick: "test", DiscordID: 1234567890}, contentType: appJson},
		{body: `{"nick":"test2"}`, status: http.StatusAccepted, key: Key{ID: "abcd", Priority: 100, Nick: "test2", DiscordID: 1234567890}, contentType: appJson},
		{body: `{"discord_id":"1"}`, status: http.StatusAccepted, key: Key{ID: "abcd", Priority: 100, Nick: "test2"}, contentType: appJson},
	}

	for _, e := range expectations {
		r = httptest.NewRequest("PUT", "http://example.com/api/key/abcd", io.NopCloser(strings.NewReader(e.body)))
		w = httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res = w.Result()
		if res.StatusCode != e.status {
			t.Fatalf("Wrong status, expected %v, got %v. Body was %s", e.status, res.StatusCode, e.body)
		}

		contentType := res.Header.Get("Content-Type")
		if contentType != e.contentType {
			t.Fatalf("Wrong Content-Type, should be %v, was %v. Body was %s", e.contentType, contentType, e.body)
		}

		if foundKey, ok := h.Keys.GetKey("abcd"); ok {
			if foundKey != e.key {
				t.Fatalf("Unexpected key afer body '%s'. Expected %v, got %v", e.body, e.key, foundKey)
			}
		} else {
			t.Fatalf("Key not found after sending body %s", e.body)
		}
	}
}

func TestKeyHandlerActiveWrite(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{State: LIVE})

	r := httptest.NewRequest("PUT", "http://example.com/api/key/active", io.NopCloser(strings.NewReader(`{"nick":"Test"}`)))
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusMethodNotAllowed {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusMethodNotAllowed, res.StatusCode)
	}
}

func TestKeyHandlerPostToKey(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{State: LIVE})

	r := httptest.NewRequest("POST", "http://example.com/api/key/abcd", io.NopCloser(strings.NewReader(`{"nick":"Test"}`)))
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusMethodNotAllowed {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusMethodNotAllowed, res.StatusCode)
	}
}

func TestKeyHandlerDeleteKey(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{})

	r := httptest.NewRequest("DELETE", "http://example.com/api/key/abcd", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	if _, ok := h.Keys.GetKey("abcd"); ok {
		t.Fatalf("Key still exists after delete")
	}

	r = httptest.NewRequest("DELETE", "http://example.com/api/key/abcd", nil)
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res = w.Result()

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusNotFound, res.StatusCode)
	}
}

func TestKeyHandlerDeleteActiveKey(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{State: LIVE})

	r := httptest.NewRequest("DELETE", "http://example.com/api/key/abcd", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusLocked, res.StatusCode)
	}

	if _, ok := h.Keys.GetKey("abcd"); !ok {
		t.Fatalf("Key was deleted")
	}
}

func TestDisconnectBadMethods(t *testing.T) {
	methods := []string{"GET", "HEAD", "POST", "OPTIONS", "CONNECT"}
	keys := Keys{}
	queue := make(chan string, 8)
	defer close(queue)
	h := KeyHandler{Keys: &keys, DisconnectQueue: queue}
	h.Keys.StoreKey("abcd", Key{})

	for _, method := range methods {
		r := httptest.NewRequest(method, "http://example.com/api/key/abcd/disconnect", nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()
		if res.StatusCode != http.StatusMethodNotAllowed {
			t.Fatalf("%s: Expected %v, got %v", method, http.StatusMethodNotAllowed, res.StatusCode)
		}
	}
}

func TestDisconnect(t *testing.T) {
	keys := Keys{}
	keys.StoreKey("abcd", Key{})
	queue := make(chan string, 8)
	defer close(queue)

	h := KeyHandler{Keys: &keys, DisconnectQueue: queue}
	r := httptest.NewRequest("PUT", "http://example.com/api/key/abcd/disconnect", nil)
	w := httptest.NewRecorder()

	h.ServeHTTP(w, r)
	res := w.Result()

	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	h.Keys.StoreKey("abcd", Key{State: LIVE})

	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != PENDING_DISCONNECT {
			t.Fatalf("State not as expected, should be %v, got %v", PENDING_DISCONNECT, foundKey.State)
		}
	} else {
		t.Fatal("Key not found")
	}

	select {
	case id := <-queue:
		if id != "abcd" {
			t.Fatalf("Wrong id sent to queue, expected abcd, got %v", id)
		}
	default:
		t.Fatal("queue empty")
	}

	r = httptest.NewRequest("PUT", "http://example.com/api/key/abcd/disconnect", io.NopCloser(strings.NewReader(`{"cooldown":"foo"}`)))
	w = httptest.NewRecorder()
	h.Keys.StoreKey("abcd", Key{State: LIVE})
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	// old style
	r = httptest.NewRequest("PUT", "http://example.com/api/disconnect/abcd?cooldown=5s", nil)
	w = httptest.NewRecorder()
	h.Keys.StoreKey("abcd", Key{State: LIVE})
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	r = httptest.NewRequest("PUT", "http://example.com/api/disconnect/abcd", nil)
	w = httptest.NewRecorder()
	h.Keys.StoreKey("abcd", Key{State: LIVE})
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	r = httptest.NewRequest("PUT", "http://example.com/api/key/abcd/disconnect/abcd", io.NopCloser(strings.NewReader(`{"cooldown":"5s"}`)))
	w = httptest.NewRecorder()
	h.Keys.StoreKey("abcd", Key{State: LIVE})
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != PENDING_DISCONNECT {
			t.Fatalf("State not as expected, should be %v, got %v", PENDING_DISCONNECT, foundKey.State)
		}
		if foundKey.Cooldown == (Key{}.Cooldown) {
			t.Fatal("Cooldown not set")
		}
	} else {
		t.Fatal("Key not found")
	}

	select {
	case id := <-queue:
		if id != "abcd" {
			t.Fatalf("Wrong id sent to queue, expected abcd, got %v", id)
		}
	default:
		t.Fatal("queue empty")
	}

	r = httptest.NewRequest("PUT", "http://example.com/api/key/abcd/disconnect/abcd", io.NopCloser(strings.NewReader(`{"force":true}`)))
	w = httptest.NewRecorder()
	h.Keys.StoreKey("abcd", Key{State: LIVE})
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != IDLE {
			t.Fatalf("State not as expected, should be %v, got %v", IDLE, foundKey.State)
		}
	} else {
		t.Fatal("Key not found")
	}

	select {
	case id := <-queue:
		if id != "abcd" {
			t.Fatalf("Wrong id sent to queue, expected abcd, got %v", id)
		}
	default:
		t.Fatal("queue empty")
	}

	r = httptest.NewRequest("DELETE", "http://example.com/api/key/abcd/disconnect", nil)
	w = httptest.NewRecorder()
	h.Keys.StoreKey("abcd", Key{})
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusAccepted {
		t.Fatalf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	if _, ok := h.Keys.GetKey("abcd"); ok {
		t.Fatal("Key wasn't removed")
	}

	r = httptest.NewRequest("DELETE", "http://example.com/api/key/abcd/disconnect", nil)
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected %v, got %v", http.StatusNotFound, res.StatusCode)
	}

	r = httptest.NewRequest("PUT", "http://example.com/api/key/abcd/disconnect", nil)
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Expected %v, got %v", http.StatusNotFound, res.StatusCode)
	}

	r = httptest.NewRequest("PUT", "http://example.com/api/", nil)
	w = httptest.NewRecorder()
	h.ServeHTTP(w, r)
	res = w.Result()

	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("PUT with bad path, expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}
}

func TestDisconnectActiveKeyByName(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{State: LIVE})

	r := httptest.NewRequest("PUT", "http://example.com/api/key/active/disconnect", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}
}

func TestDisconnectBadJSON(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{State: LIVE})

	r := httptest.NewRequest("PUT", "http://example.com/api/key/abcd/disconnect", io.NopCloser(strings.NewReader("{")))
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}
}

func TestDisconnectBadGet(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}
	h.Keys.StoreKey("abcd", Key{State: LIVE})

	r := httptest.NewRequest("GET", "http://example.com/api/key/abcd/disconnect", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()

	if res.StatusCode != http.StatusMethodNotAllowed {
		t.Fatalf("Wrong status code, expected %v, got %v", http.StatusMethodNotAllowed, res.StatusCode)
	}
}

func TestRTMPDisconnect(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	}))
	defer ts.Close()

	queue := RTMPDisconnectChannel(ts.URL)
	queue <- "abcd"
	close(queue)
}

func TestReset(t *testing.T) {
	keys := Keys{}
	h := KeyHandler{Keys: &keys}

	expectedStatus := http.StatusLocked
	expectedCooldown := (Key{}.Cooldown)
	expectedState := State(LIVE)
	expectedPendingCooldown := (Key{}.PendingCooldown)
	method := "PUT"
	keyID := "abcd"
	checkKey := true

	testFunc := func(t *testing.T) {
		r := httptest.NewRequest(method, "http://example.com/api/key/"+keyID+"/reset", nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		res := w.Result()

		if res.StatusCode != expectedStatus {
			t.Fatalf("Wrong status code, expected %v, got %v", expectedStatus, res.StatusCode)
		}

		if !checkKey {
			return
		}

		if foundKey, ok := h.Keys.GetKey("abcd"); ok {
			if foundKey.State != expectedState {
				t.Fatalf("Wrong state, expected %v, got %v", expectedState, foundKey.State)
			}
			if foundKey.Cooldown != expectedCooldown {
				t.Fatalf("Wrong cooldown, expected %v, got %v", expectedCooldown, foundKey.Cooldown)
			}
			if foundKey.PendingCooldown != expectedPendingCooldown {
				t.Fatalf("Wrong cooldown, expected %v, got %v", expectedPendingCooldown, foundKey.PendingCooldown)
			}
		} else {
			t.Fatal("Key not found")
		}
	}

	h.Keys.StoreKey("abcd", Key{State: LIVE})
	t.Run("live key", testFunc)

	expectedStatus = http.StatusAccepted

	h.Keys.StoreKey("abcd", Key{Cooldown: time.Now().Add(time.Duration(5 * time.Minute))})
	expectedState = State(IDLE)
	t.Run("idle key", testFunc)

	h.Keys.StoreKey("abcd", Key{State: PENDING_START, PendingCooldown: time.Now().Add(time.Duration(5 * time.Minute))})
	t.Run("Pending start key", testFunc)

	expectedStatus = http.StatusMethodNotAllowed

	method = "GET"
	t.Run("GET request", testFunc)

	method = "HEAD"
	t.Run("HEAD request", testFunc)

	method = "POST"
	t.Run("POST request", testFunc)

	method = "OPTIONS"
	t.Run("OPTIONS request", testFunc)

	method = "PUT"
	expectedStatus = http.StatusNotFound
	keyID = "defg"
	checkKey = false
	t.Run("Unknown key", testFunc)

	expectedStatus = http.StatusBadRequest
	keyID = "active"
	t.Run("active key", testFunc)
}
