package skm

import (
	"bytes"
	"encoding/json"
	"testing"
)

type flakes struct {
	marshalled string
	snowflake  Snowflake
}

var expectedFlakes []flakes

func TestSnowflakeMarshal(t *testing.T) {
	for _, f := range expectedFlakes {
		str, err := json.Marshal(f.snowflake)
		if err != nil {
			t.Fatal(err)
		}

		if !bytes.Equal(str, bytes.NewBufferString(f.marshalled).Bytes()) {
			t.Fatalf("Expected %v, got %v", f.marshalled, str)
		}
	}
}

func TestSnowflakeUnmarshal(t *testing.T) {
	for _, f := range expectedFlakes {
		var s Snowflake
		bytes := bytes.NewBufferString(f.marshalled).Bytes()

		if err := json.Unmarshal(bytes, &s); err != nil {
			t.Fatal(err)
		}

		if s != f.snowflake {
			t.Fatalf("Expected %v, got %v", f.snowflake, s)
		}
	}
}

func setupFlakes() {
	expectedFlakes = []flakes{
		{`"0"`, 0},
		{`"4096"`, 4096},
		{`"18446744073709551615"`, 18446744073709551615},
	}
}
