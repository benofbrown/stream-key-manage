package skm

import (
	"log"
	"net/http"
	"time"
)

type PublishHandler struct {
	DisconnectQueue     chan string
	Keys                *Keys
	WebhookQueue        chan bool
	PendingClear        chan bool
	ConnectCooldownTime time.Duration
}

type PublishDoneHandler struct {
	Keys         *Keys
	Logger       *Logger
	PendingClear chan bool
}

func NewPublishHandler(keys *Keys, disconnectQueue chan string, pendingClear chan bool) PublishHandler {
	h := PublishHandler{Keys: keys, DisconnectQueue: disconnectQueue, PendingClear: pendingClear}
	h.ConnectCooldownTime = ConnectPendingCooldownTime
	return h
}

func (h PublishHandler) drainClearQueue() {
	for {
		select {
		case <-h.PendingClear:
		default:
			return
		}
	}
}

func (h PublishHandler) takeoverKey(ID string, activeKey, keyData Key, w http.ResponseWriter) {
	err := h.Keys.DisconnectKey(activeKey.ID, h.DisconnectQueue, time.Now().Add(DefaultCooldownTime), false)
	if err != nil {
		log.Println(err)
	}
	keyData.State = PENDING_START
	keyData.PendingCooldown = time.Now().Add(PendingCooldownTime)
	h.Keys.StoreKey(ID, keyData)
	timer := time.NewTimer(h.ConnectCooldownTime)

	defer timer.Stop()
	select {
	case <-h.PendingClear:
		time.Sleep(50 * time.Millisecond)
		h.goLive(ID, keyData)
		return
	case <-timer.C:
	}

	h.drainClearQueue()
	w.WriteHeader(http.StatusLocked)
}

func (h PublishHandler) goLive(ID string, keyData Key) {
	keyData.State = LIVE
	keyData.LiveSince = time.Now()
	keyData.Cooldown = time.Time{}
	keyData.PendingCooldown = time.Time{}
	h.Keys.StoreKey(ID, keyData)
	h.drainClearQueue()
	lastKeyID := Server.GetLastKeyID()
	log.Printf("LastKeyID = %q, ID = %q", lastKeyID, ID)
	if h.WebhookQueue != nil {
		if keyData.Priority == 0 {
			h.WebhookQueue <- false
		} else if keyData.Priority <= LowPriorityThreshold {
			if lastKeyID == ID {
				h.WebhookQueue <- false
			} else {
				Server.SetLastKeyID(ID)
				h.WebhookQueue <- true
			}
		} else {
			Server.SetLastKeyID(ID)
			h.WebhookQueue <- true
		}
	}
}

func (h PublishHandler) handleActiveKey(ID string, keyData Key, w http.ResponseWriter) {
	if activeKey, activeFound := h.Keys.GetActiveKey(); activeFound {
		if keyData != activeKey {
			if keyData.Priority > activeKey.Priority {
				h.takeoverKey(ID, activeKey, keyData, w)
				return
			}

			w.WriteHeader(http.StatusLocked)
			return
		}

		if keyData.State == LIVE {
			return
		}

		if keyData.State == PENDING_DISCONNECT {
			w.WriteHeader(http.StatusLocked)
			return
		}
	} else {
		h.goLive(ID, keyData)
	}
}

func (h PublishHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	key := r.PostForm.Get("name")
	if key == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	LogRequest(r)

	if keyData, ok := h.Keys.GetKey(key); !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	} else {
		if keyData.Cooldown.After(time.Now()) {
			w.WriteHeader(http.StatusLocked)
			return
		}
		if pendingKey, ok := h.Keys.GetPendingKey(); ok {
			if pendingKey == keyData {
				h.handleActiveKey(key, keyData, w)
				return
			} else if pendingKey.PendingCooldown.Before(time.Now()) {
				pendingKey.State = IDLE
				h.Keys.StoreKey(pendingKey.ID, pendingKey)
			} else {
				w.WriteHeader(http.StatusLocked)
				return
			}
		}

		h.handleActiveKey(key, keyData, w)
		return
	}
}

func (h PublishDoneHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	key := r.PostForm.Get("name")
	if key == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	LogRequest(r)

	if keyData, ok := h.Keys.GetKey(key); ok {
		if h.Logger != nil {
			h.Logger.LogSession(keyData)
		}

		keyData.State = IDLE
		keyData.LiveSince = time.Time{}
		h.Keys.StoreKey(key, keyData)
		if h.PendingClear != nil {
			h.PendingClear <- true
		}
	}

	w.WriteHeader(http.StatusOK)
}
