package skm

import (
	"fmt"
	"log"
	"net/http"
)

func RTMPDisconnectChannel(server string) chan string {
	out := make(chan string, 8)
	urlFormat := "%s/control/drop/publisher?app=live&name=%s"
	go func() {
		for {
			id, ok := <-out
			if !ok {
				log.Println("Disconnect queue closed")
				return
			}
			url := fmt.Sprintf(urlFormat, server, id)
			_, err := http.Get(url)
			log.Println("Sent request to", url)
			if err != nil {
				log.Println(err)
			}
		}
	}()
	return out
}
