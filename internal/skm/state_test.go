package skm

import (
	"bytes"
	"encoding/json"
	"os"
	"testing"
)

type states struct {
	marshalled string
	state      State
}

var expectedStates []states

func TestMarshalAllState(t *testing.T) {
	for _, s := range expectedStates {
		state, err := json.Marshal(s.state)
		if err != nil {
			t.Fatal(err)
		}

		if !bytes.Equal(state, bytes.NewBufferString(s.marshalled).Bytes()) {
			t.Fatalf("Expected %v, got %v", s.marshalled, state)
		}
	}
}

func TestUnmarshalAllState(t *testing.T) {
	for _, s := range expectedStates {
		var state State
		if err := json.Unmarshal(bytes.NewBufferString(s.marshalled).Bytes(), &state); err != nil {
			t.Fatal(err)
		}
		if state != s.state {
			t.Fatalf("%s: Expected: %v, got: %v", s.marshalled, s.state, state)
		}
	}
}

func TestMain(m *testing.M) {
	expectedStates = []states{
		{`"IDLE"`, IDLE},
		{`"PENDING_START"`, PENDING_START},
		{`"LIVE"`, LIVE},
		{`"PENDING_DISCONNECT"`, PENDING_DISCONNECT},
		{`"UNKNOWN"`, UNKNOWN}}
	setupFlakes()
	os.Exit(m.Run())
}
