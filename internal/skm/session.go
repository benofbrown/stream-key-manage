package skm

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"os"
	"time"
)

func writeSessionFile(sessionPath string, keys *Keys) (bool, error) {
	saveRequired := Server.SaveRequired()
	if !saveRequired {
		return false, nil
	}
	newSessionPath := sessionPath + ".new"
	if newSessionFile, err := os.Create(newSessionPath); err != nil {
		return false, errors.New("Could not save to new session file: " + err.Error())
	} else {
		savedKeys := map[string]Key{}
		keys.Range(func(k, v interface{}) bool {
			key := v.(Key)
			key.ID = ""
			key.State = IDLE
			savedKeys[k.(string)] = key
			return true
		})
		encoder := json.NewEncoder(newSessionFile)
		encoder.SetIndent("", "  ")
		if err := encoder.Encode(savedKeys); err != nil {
			return false, errors.New("Could not encode savedKeys: " + err.Error())
		} else {
			newSessionFile.Close()
			if err := os.Rename(newSessionPath, sessionPath); err != nil {
				return false, errors.New("Could not rename session file: " + err.Error())
			}
		}
		Server.UnsetSaveRequired()
		return true, nil
	}
}

func SaveSessions(sessionPath string, keys *Keys) {
	tick := time.Tick(30 * time.Second)
	for range tick {
		if _, err := writeSessionFile(sessionPath, keys); err != nil {
			log.Println(err)
		}
	}
}

func ParseSessions(sessionFile io.Reader, keys *Keys) error {
	var keyMap map[string]Key
	decoder := json.NewDecoder(sessionFile)
	if err := decoder.Decode(&keyMap); err == nil {
		for k, v := range keyMap {
			v.State = IDLE
			v.LiveSince = time.Time{}
			keys.StoreKey(k, v)
		}
		return nil
	} else {
		log.Println("WARNING: Could not parse session file:", err)
		return errors.New("Could not parse session file")
	}
}

func LoadSessions(sessionPath string, keys *Keys) {
	if sessionFile, err := os.Open(sessionPath); err == nil {
		if err := ParseSessions(sessionFile, keys); err != nil {
			log.Println("WARNING: Could not parse session file")
		}
		sessionFile.Close()
	} else {
		log.Println("WARNING: Could not open session file", sessionPath)
	}
}
