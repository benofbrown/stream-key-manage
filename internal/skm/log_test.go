package skm

import (
	"bytes"
	"fmt"
	"log"
	"strings"
	"testing"
	"time"
)

func TestLogging(t *testing.T) {
	path := fmt.Sprintf("%s/session.log", t.TempDir())
	logger := LogSetup(path)

	logger.LogSession(Key{ID: "abcd", LiveSince: time.Now().Add(-5 * time.Minute)})
	logger.Rotate()
}

func TestLoggingEmptyLogger(t *testing.T) {
	logger := Logger{}

	logger.LogSession(Key{})
	logger.Rotate()
}

func TestLoggerOutput(t *testing.T) {
	logger := Logger{}

	var buf bytes.Buffer
	logger.logger = log.New(&buf, "", 0)
	key := Key{ID: "abcd", Nick: "test1", LiveSince: time.Date(2021, 1, 31, 17, 1, 1, 0, time.UTC)}

	logger.LogSession(key)

	if !strings.Contains(buf.String(), "Session ended for abcd (test1), ran from 2021-01-31 17:01:01 to ") {
		t.Fatal("Unexpected log message:", buf.String())
	}
}
