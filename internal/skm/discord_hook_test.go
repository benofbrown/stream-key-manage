package skm

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestWebhookSend(t *testing.T) {
	config := discordConfig{}

	expectedContent := "blorp"

	response := make(chan error, 8)
	defer close(response)

	forceError := false

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if forceError {
			w.WriteHeader(http.StatusBadRequest)
			response <- nil
			return
		}
		content := r.FormValue("content")
		if content == "" {
			response <- errors.New("Empty content")
			return
		}

		if content != expectedContent {
			response <- fmt.Errorf("Payload not as expected, wanted '%v', got '%v'", expectedContent, content)
		}

		response <- nil
	}))
	defer ts.Close()

	client := ts.Client()
	config.WebhookUrl = ts.URL

	queue := config.CreateWebookQueue(client)
	defer close(queue)

	t.Run("ServerOK", func(t *testing.T) {
		queue <- true

		timeout := time.After(time.Second)
		select {
		case err := <-response:
			if err != nil {
				t.Fatal(err)
			}
		case <-timeout:
			t.Fatal("Timeout out waiting for server to process request")
		}
	})
	t.Run("NoSend", func(t *testing.T) {
		queue <- false

		timeout := time.After(50 * time.Millisecond)
		select {
		case <-response:
			t.Fatal("Request was sent to the server, it should not have been")
		case <-timeout:
			t.Log("Timed out as expected")
		}
	})
	t.Run("ServerBadRequest", func(t *testing.T) {
		forceError = true
		queue <- true

		timeout := time.After(time.Second)
		select {
		case err := <-response:
			if err != nil {
				t.Fatal(err)
			}
		case <-timeout:
			t.Fatal("Timeout out waiting for server to process request")
		}
	})

	ts.Close()
	t.Run("ServerDown", func(t *testing.T) {
		forceError = true
		queue <- true

		timeout := time.After(50 * time.Millisecond)
		select {
		case <-response:
			t.Fatal("Server handled response, it shouldn't have")
		case <-timeout:
			t.Log("Timed out as expected")
		}
	})
}

func TestWebhookLoadConfig(t *testing.T) {
	expectedConfig := discordConfig{WebhookUrl: "http://example.com/hook"}
	rawConfig := io.NopCloser(strings.NewReader("<INVALID>"))

	if _, err := LoadDiscordConfig(rawConfig); err == nil {
		t.Fatal("Expected error, none received")
	}

	rawConfig = io.NopCloser(strings.NewReader(`{"webhook_url": ""}`))
	if _, err := LoadDiscordConfig(rawConfig); err == nil {
		t.Fatal("Expected error, none received")
	}

	rawConfig = io.NopCloser(strings.NewReader(`{"webhook_url": "http://example.com/hook"}`))

	if config, err := LoadDiscordConfig(rawConfig); err != nil {
		t.Fatal(err)
	} else {
		if config != expectedConfig {
			t.Fatalf("Config not as expected: %v got: %v", expectedConfig, config)
		}
	}
}
