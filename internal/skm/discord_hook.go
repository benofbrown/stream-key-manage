package skm

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
)

type discordConfig struct {
	WebhookUrl string `json:"webhook_url"`
}

func (d discordConfig) CreateWebookQueue(client *http.Client) chan bool {
	out := make(chan bool, 8)
	go func() {
		for {
			send, ok := <-out
			if !ok {
				log.Println("Discord notification channel closed")
				return
			}
			if !send {
				log.Println("Not sending webhook")
				continue
			}
			if err := d.sendPayload(client); err == nil {
				log.Println("Webhook called successfully")
			} else {
				log.Println("Webhook for failed:", err)
			}
		}
	}()
	return out
}

func (d discordConfig) sendPayload(client *http.Client) error {
	var buf bytes.Buffer
	w := multipart.NewWriter(&buf)
	if err := w.WriteField("content", "blorp"); err != nil {
		return err
	}
	w.Close()

	if res, err := client.Post(d.WebhookUrl, w.FormDataContentType(), &buf); err == nil {
		res.Body.Close()

		if res.StatusCode >= 200 && res.StatusCode < 300 {
			return nil
		}

		return fmt.Errorf("Unexpected status: %s", res.Status)
	} else {
		return err
	}
}

func LoadDiscordConfig(configFile io.ReadCloser) (discordConfig, error) {
	var discordConf discordConfig
	decoder := json.NewDecoder(configFile)
	err := decoder.Decode(&discordConf)
	if err != nil {
		return discordConf, err
	}

	if discordConf.WebhookUrl == "" {
		return discordConf, fmt.Errorf("WebhookUrl not in config")
	}

	return discordConf, nil
}
