package skm

import (
	"sync"
)

type ServerType struct {
	sync.Map
}

const (
	ALREADY_ACTIVE = 1
	SAVE_REQUIRED  = 2
	LAST_ID        = 3
)

var Server ServerType

func (s *ServerType) SetActive() {
	s.Store(ALREADY_ACTIVE, true)
}

func (s *ServerType) GetBool(id int) bool {
	if val, ok := s.Load(id); ok {
		return val.(bool)
	}

	return false
}

func (s *ServerType) GetActive() bool {
	return s.GetBool(ALREADY_ACTIVE)
}

func (s *ServerType) SetSaveRequired() {
	s.Store(SAVE_REQUIRED, true)
}

func (s *ServerType) UnsetSaveRequired() {
	s.Store(SAVE_REQUIRED, false)
}

func (s *ServerType) SaveRequired() bool {
	return s.GetBool(SAVE_REQUIRED)
}

func (s *ServerType) GetLastKeyID() string {
	if val, ok := s.Load(LAST_ID); ok {
		return val.(string)
	}

	return ""
}

func (s *ServerType) SetLastKeyID(id string) {
	s.Store(LAST_ID, id)
}
