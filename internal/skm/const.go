package skm

import (
	"time"
)

const (
	DefaultCooldownTime           time.Duration = 5 * time.Minute
	PendingCooldownTime           time.Duration = 1 * time.Minute
	ConnectPendingCooldownTime    time.Duration = 4200 * time.Millisecond
	DisconnectPendingCooldownTime time.Duration = 1 * time.Minute
	LowPriorityThreshold          int           = 11
)
