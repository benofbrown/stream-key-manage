package skm

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestUpdateBadForm(t *testing.T) {
	keys := Keys{}
	h := UpdateHandler{Keys: &keys}

	r := httptest.NewRequest("POST", "http://example.com/update", nil)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Should be %v, is %v", http.StatusBadRequest, res.StatusCode)
	}
}

func TestUpdateBadKeyId(t *testing.T) {
	keys := Keys{}
	h := UpdateHandler{Keys: &keys}

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", "http://example.com/update", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("Should be %v, is %v", http.StatusNotFound, res.StatusCode)
	}
}

func TestUpdateIdleKey(t *testing.T) {
	keys := Keys{}
	h := UpdateHandler{Keys: &keys}

	h.Keys.StoreKey("abcd", Key{})

	form := strings.NewReader("name=abcd&time=15")
	r := httptest.NewRequest("POST", "http://example.com/update", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusOK {
		t.Fatalf("Should be %v, is %v", http.StatusOK, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != LIVE {
			t.Fatalf("State wrong, should be %v, got %v", LIVE, foundKey.State)
		}
		if foundKey.LiveSince.IsZero() {
			t.Fatal("LiveSince is not set")
		}
		now := time.Now()
		if !foundKey.LiveSince.Equal(now) && !foundKey.LiveSince.Before(now) {
			t.Fatalf("LiveSince should be before or equal to the current time")
		}
	} else {
		t.Fatal("Key not found after update")
	}

	h.Keys.StoreKey("abcd", Key{})
	form = strings.NewReader("name=abcd")
	w = httptest.NewRecorder()
	r = httptest.NewRequest("POST", "http://example.com/update", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	h.ServeHTTP(w, r)

	res = w.Result()
	if res.StatusCode != http.StatusLocked {
		t.Fatalf("Second call should be %v, is %v", http.StatusLocked, res.StatusCode)
	}
}

func TestUpdatePendingDisconnect(t *testing.T) {
	keys := Keys{}
	h := UpdateHandler{Keys: &keys}

	h.Keys.StoreKey("abcd", Key{State: PENDING_DISCONNECT})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", "http://example.com/update", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusForbidden {
		t.Fatalf("Should be %v, is %v", http.StatusForbidden, res.StatusCode)
	}

	if foundKey, ok := h.Keys.GetKey("abcd"); ok {
		if foundKey.State != IDLE {
			t.Fatalf("State wrong, should be %v, got %v", IDLE, foundKey.State)
		}
		if !foundKey.LiveSince.IsZero() {
			t.Fatal("LiveSince should be zero")
		}
	} else {
		t.Fatal("Key not found after update")
	}
}

func TestUpdateInvalidForm(t *testing.T) {
	keys := Keys{}
	h := UpdateHandler{Keys: &keys}

	h.Keys.StoreKey("abcd", Key{State: PENDING_DISCONNECT})

	form := strings.NewReader("name=abcd")
	r := httptest.NewRequest("POST", "http://example.com/update", form)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	r.Body = http.MaxBytesReader(w, r.Body, 1)
	h.ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Fatalf("Should be %v, is %v", http.StatusBadRequest, res.StatusCode)
	}
}
