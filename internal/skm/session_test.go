package skm

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"
	"time"
)

func TestLoadSessions(t *testing.T) {
	tempDir := t.TempDir()
	sessionPath := tempDir + "/session.json"
	Server.UnsetSaveRequired()

	keys := Keys{}
	cooldown := time.Date(2020, 10, 7, 10, 0, 0, 0, time.UTC)
	keys.StoreKey("abcd", Key{State: IDLE, Priority: 120, Cooldown: cooldown, Nick: "Test"})
	keys.StoreKey("defg", Key{State: LIVE, LiveSince: time.Now().Add(-1 * time.Minute)})

	if written, err := writeSessionFile(sessionPath, &keys); err != nil {
		t.Fatal("Could not write session file")
	} else if !written {
		t.Fatal("File was not written to")
	}

	if written, err := writeSessionFile(sessionPath, &keys); err != nil {
		t.Fatal("Could not write session file")
	} else if written {
		t.Fatal("Second call wrote data")
	}

	loadedKeys := Keys{}
	LoadSessions(sessionPath, &loadedKeys)

	keys.Range(func(k, v interface{}) bool {
		key := k.(string)
		val := v.(Key)

		if found, ok := loadedKeys.GetKey(key); ok {
			found.ID = ""
			val.LiveSince = time.Time{}
			val.State = IDLE
			if found != val {
				t.Fatalf("Mismatched key: expected %v got %v", val, found)
			}
		} else {
			t.Fatalf("Key %v not found in loaded keys", key)
		}
		return true
	})

}

func TestSaveBadFile(t *testing.T) {
	keys := Keys{}
	keys.StoreKey("abcd", Key{})
	if _, err := writeSessionFile("/dev/zero", &keys); err == nil {
		t.Fatal("Expected an error, none received")
	} else {
		if !strings.Contains(err.Error(), "Could not save to new session file") {
			t.Fatalf("Unexpected warning: %s", err.Error())
		}
	}
}

func TestBadLoad(t *testing.T) {
	buf := &bytes.Buffer{}
	log.SetOutput(buf)
	defer log.SetOutput(os.Stderr)

	keys := Keys{}
	LoadSessions("/nonexistent", &keys)

	logstr := strings.Trim(buf.String(), "\n")
	if !strings.Contains(logstr, "Could not open session file") {
		t.Fatalf("Unexpected warning: %s", logstr)
	}
}

func TestParseNonJson(t *testing.T) {
	fileContent := strings.NewReader("abcd")
	keys := Keys{}

	if err := ParseSessions(fileContent, &keys); err == nil {
		t.Fatal("Expected error, didn't get one")
	}
}
