module gitlab.com/benofbrown/stream-key-manage

go 1.23.1

require (
	github.com/google/uuid v1.6.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)
